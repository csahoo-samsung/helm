
{{ define "ingress.service.compile.routes" }}
{{ $base := .base }}
{{ $fullName := include "microservice.fullname" .base }}
{{ $service := .service }}
{{ $serviceName := .serviceName }}
{{ $theDic := (dict "serviceName" .serviceName "service" $service "base" $base ) }}

{{ include "ingress.service.compile.routes.paths" $theDic }}
{{ include "ingress.service.compile.routes.pathPrefixes" $theDic }}
{{ if $service.forwardAuth }}
{{ if $service.forwardAuth.unprotectedPaths }}
{{ include "ingress.service.compile.routes.forwardAuth.unprotectedPaths" $theDic }}
{{ end }}
{{ if $service.forwardAuth.unprotectedPathPrefixes }}
{{ include "ingress.service.compile.routes.forwardAuth.unprotectedPathPrefixes" $theDic }}
{{ end }}
{{ end }}
{{ include "ingress.service.compile.routes.no-paths" $theDic }}
{{ include "ingress.service.compile.routes.websocket-upgrade" $theDic }}
{{ include "ingress.service.compile.routes.tcp" $theDic }}
{{ include "ingress.service.compile.routes.docs" $theDic }}

{{ end }}

{{ define "ingress.service.compile.routes.forwardAuth.unprotectedPaths" }}
{{ $base := .base }}
{{ $fullName := include "microservice.fullname" .base }}
{{ $service := .service }}
{{ $serviceName := .serviceName }}

{{ $and := ternary "" " && " (kindIs "invalid" $service.domain) }}
{{- range $service.forwardAuth.unprotectedPaths }}
{{ $item := . }}
{{ $value := $item.value}}
{{ $middlewares := $item.middlewares }}
- kind: Rule
  match: {{ if $service.domain }}Host(`{{ $service.domain }}`){{if or $service.prefix (and (not $service.disableServiceVersioning) $base.Values.service.version) }}{{ $and }}{{ end }}Path(`{{ if $service.prefix }}/{{ $service.prefix }}{{end}}{{ if (and (not $service.disableServiceVersioning) $base.Values.service.version) }}/{{ $base.Values.service.version }}{{ end }}{{ $value }}`){{ end }}{{ if $item.method }} && Method(`{{ $item.method }}`) {{ end }}
  services:
    - name: {{ $fullName }}-{{ $serviceName }}
      port: 80
  {{ if or (or $service.enableCorsInterceptor $middlewares) $service.middlewares }}
  middlewares: {{ range $middlewares }}
    - name: {{ tpl . $base }}{{ end }}
  {{ if $service.enableCorsInterceptor }}
    - name: {{ $fullName}}-cors-headers
  {{ end }}
  {{ range $service.middlewares }}
    - name: {{ tpl . $base }}
  {{ end }}
  {{ end }}
{{ end }}

{{ end }}

{{ define "ingress.service.compile.routes.paths" }}
{{ $base := .base }}
{{ $fullName := include "microservice.fullname" .base }}
{{ $service := .service }}
{{ $serviceName := .serviceName }}

{{ $and := ternary "" " && " (kindIs "invalid" $service.domain) }}
{{- range $service.paths }}
{{ $item := . }}
{{ $value := $item.value}}
{{ $middlewares := $item.middlewares }}
- kind: Rule
  match: {{ if $service.domain }}Host(`{{ $service.domain }}`){{if or $service.prefix (and (not $service.disableServiceVersioning) $base.Values.service.version) }}{{ $and }}{{ end }}Path(`{{ if $service.prefix }}/{{ $service.prefix }}{{end}}{{ if (and (not $service.disableServiceVersioning) $base.Values.service.version) }}/{{ $base.Values.service.version }}{{ end }}{{ $value }}`){{ end }}{{ if $item.method }} && Method(`{{ $item.method }}`) {{ end }}
  services:
    - name: {{ $fullName }}-{{ $serviceName }}
      port: 80
  {{ if or (or (or $service.enableCorsInterceptor $middlewares) $service.middlewares) $base.Values.traefik.forwardAuth.address }}
  middlewares: {{ range $middlewares }}
    - name: {{ tpl . $base }}{{ end }}
  {{ if $service.enableCorsInterceptor }}
    - name: {{ $fullName}}-cors-headers
  {{ end }}
  {{ range $service.middlewares }}
    - name: {{ tpl . $base }}
  {{ end }}
  {{ if $base.Values.traefik.forwardAuth.address }}
    - name: {{ include "microservice.fullname" $base }}-forward-auth
  {{ end }}
  {{ end }}
{{ end }}

{{ end }}

{{ define "ingress.service.compile.routes.forwardAuth.unprotectedPathPrefixes" }}
{{ $base := .base }}
{{ $fullName := include "microservice.fullname" .base }}
{{ $service := .service }}
{{ $serviceName := .serviceName }}

{{ $and := ternary "" " && " (kindIs "invalid" $service.domain) }}
{{- range $service.forwardAuth.unprotectedPathPrefixes }}
{{ $item := . }}
{{ $value := $item.value}}
{{ $middlewares := $item.middlewares }}
- kind: Rule
  match: {{ if $service.domain }}Host(`{{ $service.domain }}`){{ end }}{{if or $service.prefix (and (not $service.disableServiceVersioning) $base.Values.service.version) }}{{ $and }}PathPrefix(`{{ if $service.prefix }}/{{ $service.prefix }}{{end}}{{ if (and (not $service.disableServiceVersioning) $base.Values.service.version) }}/{{ $base.Values.service.version }}{{ end }}{{ $value }}`){{ end }}{{ if $item.method }} && Method(`{{ $item.method }}`) {{ end }}
  services:
    - name: {{ $fullName }}-{{ $serviceName }}
      port: {{ $service.port | default 80  }}
  {{ if or (or $service.enableCorsInterceptor $middlewares) $service.middlewares }}
  middlewares: {{ range $middlewares }}
    - name: {{ tpl . $base }}{{ end }}
  {{ if $service.enableCorsInterceptor }}
    - name: {{ $fullName}}-cors-headers
  {{ end }}
  {{ range $service.middlewares }}
    - name: {{ tpl . $base }}
  {{ end }}
  {{ end }}
{{ end }}

{{ end }}

{{ define "ingress.service.compile.routes.pathPrefixes" }}
{{ $base := .base }}
{{ $fullName := include "microservice.fullname" .base }}
{{ $service := .service }}
{{ $serviceName := .serviceName }}

{{ $and := ternary "" " && " (kindIs "invalid" $service.domain) }}
{{- range $service.pathPrefixes }}
{{ $item := . }}
{{ $value := $item.value}}
{{ $middlewares := $item.middlewares }}
- kind: Rule
  match: {{ if $service.domain }}Host(`{{ $service.domain }}`){{ end }}{{if or $service.prefix (and (not $service.disableServiceVersioning) $base.Values.service.version) }}{{ $and }}PathPrefix(`{{ if $service.prefix }}/{{ $service.prefix }}{{end}}{{ if (and (not $service.disableServiceVersioning) $base.Values.service.version) }}/{{ $base.Values.service.version }}{{ end }}{{ $value }}`){{ end }}{{ if $item.method }} && Method(`{{ $item.method }}`) {{ end }}
  services:
    - name: {{ $fullName }}-{{ $serviceName }}
      port: {{ $service.port | default 80  }}
  {{ if or (or (or $service.enableCorsInterceptor $middlewares) $service.middlewares) $base.Values.traefik.forwardAuth.address }}
  middlewares: {{ range $middlewares }}
    - name: {{ tpl . $base }}{{ end }}
  {{ if $service.enableCorsInterceptor }}
    - name: {{ $fullName}}-cors-headers
  {{ end }}
  {{ range $service.middlewares }}
    - name: {{ tpl . $base }}
  {{ end }}
  {{ if $base.Values.traefik.forwardAuth.address }}
    - name: {{ include "microservice.fullname" $base }}-forward-auth
  {{ end }}
  {{ end }}
{{ end }}

{{ end }}

{{ define "ingress.service.compile.routes.no-paths" }}
{{ $base := .base }}
{{ $fullName := include "microservice.fullname" .base }}
{{ $service := .service }}
{{ $serviceName := .serviceName }}

{{ if $service.domain }}
{{ if $service.pathPrefixes }}{{ else }}
{{ if $service.paths }}{{ else }}
- kind: Rule
  match: Host(`{{ $service.domain }}`)
  services:
    - name: {{ $fullName }}-{{ $serviceName }}
      port: {{ $service.port | default 80  }}
  {{ if $service.enableCorsInterceptor }}
  middlewares:
    - name: {{ $fullName}}-cors-headers
  {{ end }}
{{ end }}
{{ end }}
{{ end }}
{{ end }}

{{ define "ingress.service.compile.routes.websocket-upgrade" }}
{{ $fullName := include "microservice.fullname" .base }}
{{ $service := .service }}
{{ $serviceName := .serviceName }}
{{ $base := .base }}

{{ if and $service.websocketHttpUpgradeEnabled $service.domain }}
- kind: Rule
  match: Host(`{{ $service.domain }}`)
  services:
    - name: {{ $fullName }}-{{ $serviceName }}
      port: 80
  {{ if $service.enableCorsInterceptor }}
  middlewares:
    - name: {{ $fullName}}-cors-headers
  {{ end }}
{{ end }}

{{ end }}

{{ define "ingress.service.compile.routes.tcp" }}
{{ $fullName := include "microservice.fullname" .base }}
{{ $service := .service }}
{{ $serviceName := .serviceName }}

{{ if or (eq ($service.ingress | default "HTTP") "UDP") (eq ($service.ingress | default "HTTP") "TCP") }}
- kind: Rule {{ if $service.domain }}
  match: HostSNI(`{{ $service.domain }}`){{ end }}
  services:
    - name: {{ $fullName }}-{{ $serviceName }}
      port: 80
{{ end }}

{{ end }}

{{ define "ingress.service.compile.routes.docs" }}
{{ $base := .base }}
{{ $fullName := include "microservice.fullname" .base }}
{{ $service := .service }}
{{ $serviceName := .serviceName }}

{{ $and := ternary "" " && " (kindIs "invalid" $service.domain) }}
{{ if $service.docs }}
{{ if and $service.docs.prefix $service.docs.path }}
- kind: Rule
  match: {{ if $service.domain }}Host(`{{ $service.domain }}`){{ $and }}{{ end }}PathPrefix(`{{ $service.docs.prefix }}{{ if $base.Values.service.version }}/{{ $base.Values.service.version }}{{ end }}{{ $service.docs.path }}`)
  services:
    - name: {{ $fullName }}-{{ $serviceName }}
      port: {{ $service.port | default 80  }}
  {{ if $service.enableCorsInterceptor }}
  middlewares:
    - name: {{ $fullName}}-cors-headers
  {{ end }}
{{ end }}
{{ end }}

{{ end }}


