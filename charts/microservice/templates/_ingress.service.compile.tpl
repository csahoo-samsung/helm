

{{- define "ingress.service.compile" -}}
{{ $base := .base }}
{{ $fullName := include "microservice.fullname" .base }}
{{ $labels := include "microservice.labels" $base }}
{{ $extraLabels := include "microservice.extraLabels" $base }}
{{ $service := .service }}
{{ $serviceName := .serviceName }}
{{ $theDic := (dict "serviceName" .serviceName "service" $service "base" $base ) }}
---
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute{{ $service.ingress | default "" }}
metadata:
  name: {{ $fullName }}-{{ $serviceName }}{{ if $base.Values.namespace }}
  namespace: {{ $base.Values.namespace }}{{ end }}
  labels: {{ $extraLabels | nindent 4 }}{{ $labels | nindent 4 }}
spec:
  entryPoints:
    {{ if $base.Values.traefik.certResolver }}
    {{ include "ingress.service.compile.entrypoints.secure" $service | nindent 4 }}
    {{ else }}
    {{ include "ingress.service.compile.entrypoints.insecure" $service | nindent 4 }}
    {{ end }}
  routes:
    {{ include "ingress.service.compile.routes" $theDic | nindent 4 }}
    {{ include "ingress.service.compile.alt-domain.routes" $theDic | nindent 4 }}

  {{ include "ingress.service.compile.cert-resolver" $theDic | nindent 2 }}

---
{{- end -}}