{{ define "ingress.services" }}
   {{ $base := . }}
   {{ range $serviceName, $service := .Values.services }}
       {{ $theDic := (dict "serviceName" $serviceName "service" $service "base" $base ) }}

       {{ if $service.canary }}{{ else }}
           {{ include "ingress.service" $theDic }}
       {{ end }}

       {{ if $base.Values.traefik.certResolver }}
       {{ include "ingress.autoconfigure-https-redirect" $theDic }}
       {{ end }}
   {{ end }}
{{ end }}