![Purple Pie](https://s3.amazonaws.com/assets.cryptexlabs.com/pie-logo.png)

## <p align="center">Protected Personal Information Server</p>
<p align="center">Encrypt and audit log access to protected user information</p>

## Key features
- Encrypted user data store
- Attributes
- Schedules
- Contacts
- Audit logging for user information access
- Localized configuration

## Source
https://gitlab.com/cryptexlabs/public/docker/purple-pie

https://gitlab.com/cryptexlabs/public/helm/-/tree/master/charts/purple-pie

## Installation notes

### Initializing vault
By default installation will fail because for very ridiculous and obscure reasons which no one has yet to identify, vault must be initialized by pounding commands on a CLI.

#### 1. exec into pod and run initialize command
```shell
kubectl exec -it purple-pie-vault-0 -- /bin/sh
vault operator init
```

Copy the token and the keys output from the command you ran above

#### 2. port forward the svc then open in browser
```shell
kubectl port-forward pod/purple-pie-vault-0 8200:8200
```

#### 3. Enter in 3 keys

#### 4. Hope you don't have to do that again

#### 5. Exec into vault pod again

```shell
kubectl exec -it purple-pie-vault-0 -- /bin/sh
```

#### 6. Run these commands

```shell
export VAULT_TOKEN="your root account token from step 1"
vault secrets enable transit
vault secrets enable -path=encryption transit
vault write -f transit/keys/users
vault policy write purple-pie-users -<<EOF
path "transit/encrypt/users" {
   capabilities = [ "update" ]
}
path "transit/decrypt/users" {
   capabilities = [ "update" ]
}
EOF
vault token create -policy=purple-pie-users -format=json
```

#### 7. Base64 encode the `auth.client_token` from the output in last step

```shell
echo -n "your token" | base64
```

#### 8. Manually edit vault secrets

```shell
kubectl edit secret purple-pie-vault-secrets
```

#### 9. Run helm upgrade again to retry the installation