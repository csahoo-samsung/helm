# Cryptex Labs Helm
![Build Status](https://gitlab.com/cryptexlabs/public/helm/badges/master/pipeline.svg)

Public helm charts supported by Cryptex Labs

## How to use
```bash
helm repo add cryptexlabs https://helm.cryptexlabs.com
```